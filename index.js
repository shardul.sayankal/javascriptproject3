let myLeads =[]
const inputs = document.getElementById("inputBox")
const inputButton = document.getElementById("input-button")
const unordedList = document.getElementById("unordered-list")
const deleteButton = document.getElementById("delete-button")
const leadsFromLocalStorage = JSON.parse( localStorage.getItem("myLeads") )
const tabButton = document.getElementById("tab-button")

if (leadsFromLocalStorage) {
    myLeads = leadsFromLocalStorage
    render(myLeads)
}

tabButton.addEventListener("click",function(){
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
        myLeads.push(tabs[0].url)
        localStorage.setItem("myLeads", JSON.stringify(myLeads) )
        render(myLeads)
    })
})

function render(leads){
    let listItems = ""
    for (let i = 0; i< leads.length; i++){
        listItems += 
        `
            <li>
                <a target ='_blank' href='${leads[i]}'>
                    ${leads[i]}
                </a>
            </li>
            
        `
    }
    unordedList.innerHTML = listItems
}

deleteButton.addEventListener("dblclick", function() {
    localStorage.clear()
    myLeads = []
    render(myLeads)
})

inputButton.addEventListener("click", function(){
    myLeads.push(inputs.value);
    inputs.value = ""
    localStorage.setItem("myLeads",JSON.stringify(myLeads))
    render(myLeads)
})